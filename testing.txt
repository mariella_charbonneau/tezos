1) build tezos

2) prepare tezos-client

./tezos-client import secret key activator unencrypted:edsk31vznjHSSpGExDMHYASz45VZqXN4DPxvsa4hAyY8dHM28cZzp6
./tezos-client import secret key baker unencrypted:edsk3gUfUPyBSfrS9CCgmCiQsTCHGkviBDusMxDJstFtojtc1zcpsh
./tezos-client import secret key baker2 unencrypted:edsk39qAm1fiMjgmPkw1EgQYkMzkJezLNewd7PLNHTkr6w9XA2zdfo
./tezos-client import secret key baker3 unencrypted:edsk4QLrcijEffxV31gGdN2HU7UpyJjA8drFoNcmnB28n89YjPNRFm
./tezos-client import secret key baker4 unencrypted:edsk4ArLQgBTLWG5FJmnGnT689VKoqhXwmDPBuGx3z4cvwU9MmrPZZ

3) granada test:

run tezos node

./tezos-node run --data-dir ./prototest/granada/ --sandbox ./prototest/sandbox.json --rpc-addr 127.0.0.1:8732

then in a seperate terminal, we bake a few blocks, watch update to granada, and check the contract balance


/src $ ./tezos-client activate protocol PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i with fitness 1 and key activator and parameters _build/default/src/proto_009_PsFLoren/lib_parameters/sandbox-parameters.json
Warning:
  
                 This is NOT the Tezos Mainnet.
  
           Do NOT use your fundraiser keys on this network.

Injected BLgBf4dPkbaF
/src $ 
/src $ ./tezos-client bake for baker
Warning:
  
                 This is NOT the Tezos Mainnet.
  
           Do NOT use your fundraiser keys on this network.

Jun  2 10:42:52.947 - 009-PsFLoren.baking.forge: found 0 valid operations (0 refused) for timestamp 2021-06-02T10:42:52.000-00:00 (fitness 01::0000000000000001)
Injected block BMSSQmwqLSdC
/src $ curl http://127.0.0.1:8732/chains/main/blocks/head/header
{"protocol":"PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i","chain_id":"NetXdQprcVkpaWU","hash":"BMSSQmwqLSdCSPaW1X7xyC9WuBv8MA4cFwLATn1jKyZMaex5kV1","level":2,"proto":1,"predecessor":"BLgBf4dPkbaFKaTUQahuMAE5pmbWDEsUArmTd6vAhQ5PDZmaC6U","timestamp":"2021-06-02T10:42:52Z","validation_pass":4,"operations_hash":"LLoa7bxRTKaQN2bLYoitYB6bU2DvLnBAqrVjZcvJ364cTcX2PZYKU","fitness":["01","0000000000000001"],"context":"CoUvYywabmtnF3vPknZyD1ZSFDHDspysoHhD3dzaANjaU6YtRibZ","priority":4,"proof_of_work_nonce":"c8048abf00000000","signature":"sigUAcuALMqhVoYZs3s41mDGsgsHpx2dUoZ59cTpAVEG8YDiwFXBM3XYXKjkTnoGdw3Nvo1EejfkEERciDzimh1CpuufxXTf"}
/src $ ./tezos-client bake for baker
Warning:
  
                 This is NOT the Tezos Mainnet.
  
           Do NOT use your fundraiser keys on this network.

Jun  2 10:43:12.023 - 009-PsFLoren.baking.forge: found 0 valid operations (0 refused) for timestamp 2021-06-02T10:43:12.000-00:00 (fitness 01::0000000000000002)
Injected block BL3DCBVg7B55
/src $ 
/src $ ./tezos-client bake for baker
Warning:
  
                 This is NOT the Tezos Mainnet.
  
           Do NOT use your fundraiser keys on this network.

Jun  2 10:43:20.736 - 009-PsFLoren.baking.forge: found 0 valid operations (0 refused) for timestamp 2021-06-02T10:43:20.000-00:00 (fitness 01::0000000000000003)
Injected block BLPeLZnddHg2
/src $ 
/src $ ./tezos-client bake for baker
Warning:
  
                 This is NOT the Tezos Mainnet.
  
           Do NOT use your fundraiser keys on this network.

Jun  2 10:43:26.907 - 009-PsFLoren.baking.forge: found 0 valid operations (0 refused) for timestamp 2021-06-02T10:43:26.000-00:00 (fitness 01::0000000000000004)
Injected block BM9MDJyatAKR
/src $ 
/src $ ./tezos-client bake for baker
Warning:
  
                 This is NOT the Tezos Mainnet.
  
           Do NOT use your fundraiser keys on this network.

Jun  2 10:43:32.053 - 010-PtGRANAD.delegate.baking_forge: found 0 valid operations (0 refused) for timestamp
Jun  2 10:43:32.053 - 010-PtGRANAD.delegate.baking_forge:   2021-06-02T10:43:32.000-00:00 (fitness <list>)
Injected block BM8pjHdWyexD
/src $ 
/src $ ./tezos-client bake for baker
Warning:
  
                 This is NOT the Tezos Mainnet.
  
           Do NOT use your fundraiser keys on this network.

Jun  2 10:43:39.543 - 010-PtGRANAD.delegate.baking_forge: found 0 valid operations (0 refused) for timestamp
Jun  2 10:43:39.543 - 010-PtGRANAD.delegate.baking_forge:   2021-06-02T10:43:39.000-00:00 (fitness <list>)
Injected block BMZkFjYFrL8k
/src $ 
/src $ ./tezos-client bake for baker
Warning:
  
                 This is NOT the Tezos Mainnet.
  
           Do NOT use your fundraiser keys on this network.

Jun  2 10:43:45.207 - 010-PtGRANAD.delegate.baking_forge: found 0 valid operations (0 refused) for timestamp
Jun  2 10:43:45.207 - 010-PtGRANAD.delegate.baking_forge:   2021-06-02T10:43:45.000-00:00 (fitness <list>)
Injected block BL6kTNMK4vbZ
/src $ 
/src $ 
/src $ curl http://localhost:8732/chains/main/blocks/head/context/contracts/
["KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5","tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN","tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv","KT1AafHA1C1vk959wvHWBispY9Y2f3fxBUUo","KT1VqarPDicMFn1ejmQqqshUkUXTCTXwmkCN","tz1faswCTDciRzE4oJ9jn2Vm2dvjeyA9fUzU","tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx","tz1b7tUupMgCNw2cCLpKTkSD1NZzB5TkP2sv"]
/src $ 
/src $ ./tezos-client get balance for KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5
Warning:
  
                 This is NOT the Tezos Mainnet.
  
           Do NOT use your fundraiser keys on this network.

7.5001 ꜩ





4) Our protocol test

run tezos node

./tezos-node run --data-dir ./prototest/psmar --sandbox ./prototest/sandbox.json --rpc-addr 127.0.0.1:8732

then, do the same as above


/src $ ./tezos-client activate protocol PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i with fitness 1 and key activator and parameters _build/default/src/proto_009_PsFLoren/lib_parameters/sandbox-parameters.json
Warning:
  
                 This is NOT the Tezos Mainnet.
  
           Do NOT use your fundraiser keys on this network.

Injected BMFbK33wefLd
/src $ ./tezos-client bake for baker2
Warning:
  
                 This is NOT the Tezos Mainnet.
  
           Do NOT use your fundraiser keys on this network.

Jun  2 11:17:42.560 - 009-PsFLoren.baking.forge: found 0 valid operations (0 refused) for timestamp 2021-06-02T11:17:42.000-00:00 (fitness 01::0000000000000001)
Injected block BM1SJvoCJomK
/src $ 
/src $ 
/src $ ./tezos-client bake for baker2
Warning:
  
                 This is NOT the Tezos Mainnet.
  
           Do NOT use your fundraiser keys on this network.

Jun  2 11:17:52.954 - 009-PsFLoren.baking.forge: found 0 valid operations (0 refused) for timestamp 2021-06-02T11:17:52.000-00:00 (fitness 01::0000000000000002)
Injected block BMZmwEi88QrP
/src $ 
/src $ ./tezos-client bake for baker2
Warning:
  
                 This is NOT the Tezos Mainnet.
  
           Do NOT use your fundraiser keys on this network.

Jun  2 11:18:02.272 - 009-PsFLoren.baking.forge: found 0 valid operations (0 refused) for timestamp 2021-06-02T11:18:02.000-00:00 (fitness 01::0000000000000003)
Injected block BMTYsMTsFSDL
/src $ 
/src $ ./tezos-client bake for baker2
Warning:
  
                 This is NOT the Tezos Mainnet.
  
           Do NOT use your fundraiser keys on this network.

Jun  2 11:18:12.240 - 009-PsFLoren.baking.forge: found 0 valid operations (0 refused) for timestamp 2021-06-02T11:18:12.000-00:00 (fitness 01::0000000000000004)
Injected block BLby3a6u8u79
/src $ 
/src $ ./tezos-client bake for baker2
Warning:
  
                 This is NOT the Tezos Mainnet.
  
           Do NOT use your fundraiser keys on this network.

Jun  2 11:18:19.159 - 010-PsmarYW.delegate.baking_forge: found 0 valid operations (0 refused) for timestamp
Jun  2 11:18:19.159 - 010-PsmarYW.delegate.baking_forge:   2021-06-02T11:18:19.000-00:00 (fitness <list>)
Injected block BMaocjr8MoVg
/src $ 
/src $ ./tezos-client bake for baker2
Warning:
  
                 This is NOT the Tezos Mainnet.
  
           Do NOT use your fundraiser keys on this network.

Jun  2 11:18:23.451 - 010-PsmarYW.delegate.baking_forge: found 0 valid operations (0 refused) for timestamp
Jun  2 11:18:23.451 - 010-PsmarYW.delegate.baking_forge:   2021-06-02T11:18:23.000-00:00 (fitness <list>)
Injected block BLkUKiZfnhXP
/src $ 
/src $ ./tezos-client bake for baker2
Warning:
  
                 This is NOT the Tezos Mainnet.
  
           Do NOT use your fundraiser keys on this network.

Jun  2 11:18:28.246 - 010-PsmarYW.delegate.baking_forge: found 0 valid operations (0 refused) for timestamp
Jun  2 11:18:28.246 - 010-PsmarYW.delegate.baking_forge:   2021-06-02T11:18:28.000-00:00 (fitness <list>)
Injected block BMePE1nifDgn
/src $ 
/src $ ./tezos-client bake for baker2
Warning:
  
                 This is NOT the Tezos Mainnet.
  
           Do NOT use your fundraiser keys on this network.

Jun  2 11:18:34.679 - 010-PsmarYW.delegate.baking_forge: found 0 valid operations (0 refused) for timestamp
Jun  2 11:18:34.679 - 010-PsmarYW.delegate.baking_forge:   2021-06-02T11:18:34.000-00:00 (fitness <list>)
Injected block BLq5WQGyJvub
/src $ 
/src $ curl http://localhost:8732/chains/main/blocks/head/context/contracts/
["KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5","tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN","tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv","KT1AafHA1C1vk959wvHWBispY9Y2f3fxBUUo","KT1VqarPDicMFn1ejmQqqshUkUXTCTXwmkCN","tz1faswCTDciRzE4oJ9jn2Vm2dvjeyA9fUzU","tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx","tz1b7tUupMgCNw2cCLpKTkSD1NZzB5TkP2sv"]
/src $ 
/src $ ./tezos-client get balance for KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5
Warning:
  
                 This is NOT the Tezos Mainnet.
  
           Do NOT use your fundraiser keys on this network.

0.0001 ꜩ






